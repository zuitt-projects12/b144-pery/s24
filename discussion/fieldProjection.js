// Excluding fields
// db.users.find({}, {"_id": 0})
// db.users.find({ firstName: "John" },
// {
//     "firstName": 1,
//     "lastName": 1,
//     "age": 1
//     
// })

// db.users.find({ firstName: "John" },
// {
//     "_id":0,
//     firstName: 1,
//     lastName: 1    
//     
// })

// db.users.find({ firstName: "John" },
// {
//     "_id":0,
//     firstName: 1,
//     lastName: 1,    
//     "contact.phone": 1
// })

// Including fields
// db.users.find({}, {"_id": 1})

db.users.find({}, {
    _id: 0,
    courses:{$slice: [1, 2]}
    })