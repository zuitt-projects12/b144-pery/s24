// > - greater than
/*db.users.find({age: {$gt: 50}})
>= greater than or equal to
db.users.find({age: {$gte: 65}})*/

// < - less than
/*db.users.find({age: {$lt: 50}})
< - less than or equal to
db.users.find({age: {$lte: 65}})*/

// != not equal
//db.users.find({age: {$ne:50}})

// in operator
//db.users.find({lastName: {$in: ["Hawkins", "Doe"]} })
// db.users.find({courses: {$in: ["Sass", "React"]} })

// || - or operator
// db.users.find({$or: [{firstName: "Neil"}, {age: 25}] })

// && - and operator
// db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}] })

// $regex
// db.users.find({ firstName: {$regex: "N"} })
// db.users.find({ firstName: {$regex: "a"} })
// db.users.find({ lastName: {$regex: "a"} })

// regex with options
// db.users.find({ firstName: {$regex: "j", $options:"$i"} })
// db.users.find({ firstName: {$regex: "s", $options:"$i"} })